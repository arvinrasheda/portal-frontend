@extends('layouts.home')

@section('styles')
<link rel="stylesheet" href="{{ mix('/vendor/libs/datatables/datatables.css') }}">
@endsection

@section('scripts')
<!-- Dependencies -->
<script src="{{ mix('/vendor/libs/datatables/datatables.js') }}"></script>

<script src="{{ mix('/js/tables_datatables.js') }}"></script>
@endsection

@section('content')
<h4 class="font-weight-bold py-3 mb-4">
    <span class="text-muted font-weight-light"></span>Category
</h4>

<div class="card">
    <div class="card-body py-3  ">
        <div class="text-right">
            <a href="{{ route('newcat') }}" class="btn btn-primary btn-sm"><i class="ion ion-md-add mr-2"></i>Add
                Category</a>
        </div>
    </div>
    <div class="card-datatable table-responsive">
        <table class="datatables-demo table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Rendering engine</th>
                    <th>Browser</th>
                    <th>Platform(s)</th>
                    <th>Engine version</th>
                    <th>CSS grade</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr class="odd gradeX">
                    <td>Trident</td>
                    <td>Internet Explorer 4.0</td>
                    <td>Win 95+</td>
                    <td class="center"> 4</td>
                    <td class="center">X</td>
                    <td>
                        <form action="" method="POST">
                            @csrf
                            <a href="" class="btn btn-success"><i class="ion ion-md-create"></i></a>
                            <a href="" class="btn btn-primary"><i class="ion ion-md-eye"></i></a>
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="btn btn-danger"><i class="ion ion-md-trash"></i></button>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>


</div>
@endsection
