<!-- Layout sidenav -->
<div id="layout-sidenav" class="{{ isset($layout_sidenav_horizontal) ? 'layout-sidenav-horizontal sidenav-horizontal container-p-x flex-grow-0' : 'layout-sidenav sidenav-vertical' }} sidenav bg-sidenav-theme">
    @empty($layout_sidenav_horizontal)
    <!-- Brand demo (see assets/css/demo/demo.css) -->
    <div class="app-brand demo">
        <a href="/" class="app-brand-text demo sidenav-text font-weight-normal ml-2">Admin</a>
        <a href="javascript:void(0)" class="layout-sidenav-toggle sidenav-link text-large ml-auto">
            <i class="ion ion-md-menu align-middle"></i>
        </a>
    </div>

    <div class="sidenav-divider mt-0"></div>
    @endempty

    <!-- Links -->
    <ul class="sidenav-inner{{ empty($layout_sidenav_horizontal) ? ' py-1' : '' }}">

        <!-- Dashboards -->
        <li class="sidenav-item">
            <a href="javascript:void(0)" class="sidenav-link"><i class="sidenav-icon ion ion-md-speedometer"></i>
                <div>Dashboards</div>
            </a>
        </li>

        <!-- Category -->
        <li class="sidenav-item">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i class="sidenav-icon ion ion-ios-pricetag"></i>
                <div>Category</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item">
                    <a href="{{ route('category') }}" class="sidenav-link">
                        <div>All Category</div>
                    </a>
                </li>
            </ul>
        </li>
        <!-- Article -->
        <li class="sidenav-item">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i class="sidenav-icon ion ion-ios-list    "></i>
                <div>Article</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item">
                    <a href="{{ route('article') }}" class="sidenav-link">
                        <div>All Article</div>
                    </a>
                </li>
            </ul>
        </li>
        <!-- Pages -->
        <li class="sidenav-item">
            <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i class="sidenav-icon ion ion-ios-document"></i>
                <div>Pages</div>
            </a>

            <ul class="sidenav-menu">
                <li class="sidenav-item">
                    <a href="{{ route('pages') }}" class="sidenav-link">
                        <div>All Pages</div>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
<!-- / Layout sidenav -->
