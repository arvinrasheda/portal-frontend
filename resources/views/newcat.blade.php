@extends('layouts.home')

@section('content')
<div class="card mb-4">
    <h6 class="card-header">
        New Category
    </h6>
    <div class="card-body">
        <form>
            <div class="form-group">
                <label class="form-label">Name Category</label>
                <input type="tex" class="form-control" placeholder="Name Category">
            </div>
            <div class="form-group">
                <label class="form-label">Description Category</label>
                <textarea name="meta_tag_description" class="form-control" placeholder="Meta Tag Description"></textarea>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>
@endsection
