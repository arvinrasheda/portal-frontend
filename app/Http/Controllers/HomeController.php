<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function category(){
        return view('category');
    }
    public function newcat(){
        return view('newcat');
    }
    public function article(){
        return view('article');
    }
    public function pages(){
        return view('pages');
    }
    
}
